/**
 * Copyright 2004-2013 Crypto-Pro. All rights reserved.
 * Программный код, содержащийся в этом файле, предназначен
 * для целей обучения. Может быть скопирован или модифицирован
 * при условии сохранения абзацев с указанием авторства и прав.
 *
 * Данный код не может быть непосредственно использован
 * для защиты информации. Компания Крипто-Про не несет никакой
 * ответственности за функционирование этого кода.
 */
package ru.CryptoPro.AndroidTLSSample;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.URL;
import java.security.Security;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;

import ru.CryptoPro.AndroidTLSSample.util.Constants;
import ru.CryptoPro.AndroidTLSSample.util.Logger;
import ru.CryptoPro.AndroidTLSSample.util.TLSUtils;
import ru.CryptoPro.JCSP.CSPConfig;
import ru.CryptoPro.JCSP.CSPProviderInterface;
import ru.CryptoPro.JCSP.JCSP;
import ru.CryptoPro.reprov.RevCheck;
import ru.CryptoPro.ssl.Provider;
import ru.CryptoPro.ssl.util.cpSSLConfig;
import ru.cprocsp.ACSP.tools.common.AppUtils;
import ru.cprocsp.ACSP.tools.common.CSPLicenseConstants;
import ru.cprocsp.ACSP.tools.common.CSPTool;
import ru.cprocsp.ACSP.tools.license.ACSPLicense;

/**
 * Главная activity приложения. Выполнение инициализации
 * CSP и java-провайдеров, копирования тестовых контейнеров
 * и др.
 *
 * Содержит два примера TLS подключения: без клиентской
 * аутентификации и с клиентской аутентификацией.
 */
public class MainActivity extends Activity implements View.OnClickListener, Constants {

    /**
     * Объект для управления лицензией провайдера.
     */
    private ACSPLicense appLicense = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Log.i(APP_LOGGER_TAG, "Load application: " + getPackageName());

        // Версия приложения в заголовке.

        String title = getString(R.string.app_name) +
                ", v. " + AppUtils.getApplicationVersion(this);

        setTitle(title);

        // Инициализация провайдеров: CSP и java-провайдеров
        // (Обязательная часть).

        if (!initCSPProviders()) {
            Log.i(APP_LOGGER_TAG, "Couldn't initialize CSP.");
            return;
        } // if

        initJavaProviders();

        // Копирование тестовых контейнеров для TLS (Примеры и вывод
        // в лог).

        initLogger();
        installContainers();

        // Вывод информации о тестовых контейнерах.

        logTestContainers();

        // Ввод лицензии.
        // В методе installNewLicense необходимо в переменной newSerialNumber
        // задать новый номер лицензии для сохранения.

        CSPProviderInterface pi = CSPConfig.INSTANCE.getCSPProviderInfo();

        try {
            appLicense = (ACSPLicense) pi.getLicense();
            installNewLicense();
        } catch (Exception e) {

            AppUtils.errorMessage(this, getString(R.string.LicenseInvalidSerial));
            Logger.log("invalid license: " + e.getMessage());

            return;

        }

        // Кнопка запуска примера TLS.

        Button btTlsExecute = findViewById(R.id.btTlsExecute);
        btTlsExecute.setOnClickListener(this);

        // Кнопка запуска примера TLS с аут. клиента.

        Button btTlsClientAuthExecute = findViewById(R.id.btTlsClientAuthExecute);
        btTlsClientAuthExecute.setOnClickListener(this);

    }

    /**
     * Уставновка лицензии.
     *
     */
    private void installNewLicense() {

        // В переменной newSerialNumber необходимо
        // задать новый номер лицензии для сохранения.
        // Например, "5050X-80030-05WE5-0QQWR-PWQFL" - истекшая лицензия.

        String newSerialNumber = "5050X-80030-05WE5-0QQWR-PWQFL"; // истекшая лицензия

        // Сохраняем номер. Строгая проверка лицензии.

        int licenseStatus = appLicense.checkAndSave(newSerialNumber, true);

        // Если произошла ошибка при сохранении, то
        // предупреждаем.

        if (licenseStatus != CSPLicenseConstants.LICENSE_STATUS_OK) {

            String format = getString(R.string.LicenseInvalidSerial2);

            AppUtils.showWarning(MainActivity.this, String.format(format, newSerialNumber),
                    getString(R.string.Warning), getString(android.R.string.ok));

        } // if
        else {

            // Иначе обновляем дату установки, номер, статус
            // в панели и оставшийся срок.

            String format = getString(R.string.LicenseSerialInstalled);
            AppUtils.alert(MainActivity.this, String.format(format, newSerialNumber));

        } // else

    }

    @Override
    public void onClick(View v) {

        Logger.clear();

        boolean useClientAuth = v.getId() != R.id.btTlsExecute;
        TlsExampleTask exampleTask = new TlsExampleTask(MainActivity.this, useClientAuth);
        exampleTask.execute();

    }

    /**
     * Класс примера подключения.
     *
     */
    private static class TlsExampleTask extends AsyncTask<Void, Void, Void> {

        /**
         * Индикатор.
         */
        private final ProgressDialog progressDialog;

        /**
         * True, если используется клиентская
         * аутентификация.
         */
        private final boolean useClientAuth;

        /**
         * Путь к папке приложения.
         */
        private final String dataDir;

        /**
         * Конструктор.
         *
         * @param activity Активити.
         * @param useClientAuth True, если используется клиентская
         * аутентификация.
         */
        TlsExampleTask(Activity activity, boolean useClientAuth) {

            this.useClientAuth = useClientAuth;
            dataDir = activity.getApplicationInfo().dataDir;

            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(activity.getString(R.string.ProgressDialogExecuting));

        }

        @Override
        protected void onPreExecute() {

            // GUI thread.

            progressDialog.show();
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... parameter) {

            // Поток может отобразить окно запроса пароля
            // к контейнеру.

            Logger.log("Init TLS Example.");
            SSLSocket sslSocket = null;
            SSLContext sslContext;
            try {
                sslContext = TLSUtils.createSSLContext(dataDir, useClientAuth);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            try {

                Logger.log("Creating SSL socket ...");
                sslSocket  = (SSLSocket) sslContext.getSocketFactory().createSocket();

                String hostAddress = useClientAuth ? host2012256ClientAuth : host2012256NoAuth;
                URL url = new URL(hostAddress);
                InetAddress ip = InetAddress.getByName(url.getHost());

                SocketAddress socketAddress = new InetSocketAddress(ip, DEFAULT_PORT);
                sslSocket.connect(socketAddress, DEFAULT_TIMEOUT);

                Logger.log("Sends a request.");

                final InputStream in = sslSocket.getInputStream();
                final OutputStream out = sslSocket.getOutputStream();

                // отправка запроса
                String request = "GET /index.html HTTP/1.0\r\n\r\n";
                Logger.log("Request content: " + request);
                out.write(request.getBytes());
                out.flush();

                // разбор ответа
                Logger.log("Client parses answer.");
                final String answer = new String(TLSUtils.readHeader(in, http_header_separator.getBytes()));

                Logger.log(answer);
                int fileLength = TLSUtils.parseAnswer(answer);

                final byte[] body = TLSUtils.readBody(in, fileLength);
                final String bodyStr = new String(body, Constants.DEFAULT_ENCODING);

                Logger.log(bodyStr);

                Logger.log("Connection has been established (OK).");
                Logger.setStatusOK();

            } catch (Exception e) {

                Logger.log(e.getMessage());
                Logger.setStatusFailed();

                Log.e(APP_LOGGER_TAG, "Operation exception", e);

            } finally {
                if (sslSocket != null) {
                    try {
                        sslSocket.close();
                    } catch (IOException e) {}
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            // GUI thread.

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        final ScrollView svLog = findViewById(R.id.svLog);
        MenuItem item = menu.findItem(R.id.miShowLog);

        Switch swLog = item.getActionView().findViewById(R.id.swShowLog);
        swLog.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    svLog.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            1f)
                    );

                } // if
                else {

                    svLog.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, 0)
                    );

                } // else

            }
        });

        return true;

    }

    /************************ Инициализация провайдера ************************/

    /**
     * Инициализация CSP провайдера.
     *
     * @return True в случае успешной инициализации.
     */
    private boolean initCSPProviders() {

        // Инициализация провайдера CSP. Должна выполняться
        // один раз в главном потоке приложения, т.к. использует
        // статические переменные.
        //
        // Далее может быть использована версия функции инициализации:
        // 1) расширенная - initEx(): она содержит init() и дополнительно
        // выполняет загрузку java-провайдеров (Java CSP, RevCheck, Java TLS)
        // и настройку некоторых параметров, например, Java TLS;
        // 2) обычная - init(): без загрузки java-провайдеров и настройки
        // параметров.
        //
        // Для совместного использования ГОСТ и не-ГОСТ TLS НЕ следует
        // переопределять свойства System.getProperty(javax.net.*) и
        // Security.setProperty(ssl.*).
        //
        // Создаем инфраструктуру CSP и копируем ресурсы
        // в папку. В случае ошибки мы, например, выводим окошко
        // (или как-то иначе сообщаем) и завершаем работу.

        int initCode   = CSPConfig.init(this);
        boolean initOk = initCode == CSPConfig.CSP_INIT_OK;

        // Если инициализация не удалась, то сообщим об ошибке.
        if (!initOk) {

            switch (initCode) {

                // Не передан контекст приложения (null). Он необходим,
                // чтобы произвести копирование ресурсов CSP, создание
                // папок, смену директории CSP и т.п.
                case CSPConfig.CSP_INIT_CONTEXT:
                    AppUtils.errorMessage(this, "Couldn't initialize context.");
                    break;

                /**
                 * Не удается создать инфраструктуру CSP (папки): нет
                 * прав (нарушен контроль целостности) или ошибки.
                 * Подробности в logcat.
                 */
                case CSPConfig.CSP_INIT_CREATE_INFRASTRUCTURE:
                    AppUtils.errorMessage(this, "Couldn't create CSP infrastructure.");
                    break;

                /**
                 * Не удается скопировать все или часть ресурсов CSP -
                 * конфигурацию, лицензию (папки): нет прав (нарушен
                 * контроль целостности) или ошибки.
                 * Подробности в logcat.
                 */
                case CSPConfig.CSP_INIT_COPY_RESOURCES:
                    AppUtils.errorMessage(this, "Couldn't copy CSP resources.");
                    break;

                /**
                 * Не удается задать рабочую директорию для загрузки
                 * CSP. Подробности в logcat.
                 */
                case CSPConfig.CSP_INIT_CHANGE_WORK_DIR:
                    AppUtils.errorMessage(this, "Couldn't change CSP working directory.");
                    break;

                /**
                 * Неправильная лицензия.
                 */
                case CSPConfig.CSP_INIT_INVALID_LICENSE:
                    AppUtils.errorMessage(this, "Invalid CSP serial number.");
                    break;

                /**
                 * Не удается создать хранилище доверенных сертификатов
                 * для CAdES API.
                 */
                case CSPConfig.CSP_TRUST_STORE_FAILED:
                    AppUtils.errorMessage(this, "Couldn't create trust store for CAdES API.");
                    break;

                /**
                 * Не удается сохранить путь к библиотекам провайдера
                 * в конфиг.
                 */
                case CSPConfig.CSP_STORE_LIBRARY_PATH:
                    AppUtils.errorMessage(this, "Couldn't store native library path to config.");
                    break;

                /**
                 * Ошибка контроля целостности.
                 */
                case CSPConfig.CSP_INIT_INVALID_INTEGRITY:
                    AppUtils.errorMessage(this, "Integrity control failure.");
                    break;

            } // switch

        } // if

        return initOk;
    }

    /**
     * Добавление нативного провайдера Java CSP,
     * SSL-провайдера и Revocation-провайдера в
     * список Security.
     *
     * Происходит один раз при инициализации.
     * Возможно только после инициализации в CSPConfig!
     *
     */
    private void initJavaProviders() {

        // %%% Инициализация остальных провайдеров %%%

        //
        // Загрузка Java CSP (хеш, подпись, шифрование,
        // генерация контейнеров).
        //

        if (Security.getProvider(JCSP.PROVIDER_NAME) == null) {
            Security.addProvider(new JCSP());
        } // if

        //
        // Загрузка Java TLS (TLS).
        //
        // Внимание!
        // Чтобы не мешать не-ГОСТовой реализации, ряд свойств внизу *.ssl и
        // javax.net.* НЕ следует переопределять! Но при этом не исключены проблемы
        // в работе с ГОСТом там, где TLS-реализация клиента обращается к дефолтным
        // алгоритмам реализаций этих factory (особенно: apache http client или
        // HttpsURLConnection без явной передачи предварительно созданного SSLContext
        // и его SSLSocketFactory).
        // То есть если используется HttpsURLConnection + свойства хранилищ javax.net.*,
        // заданные через System.setProperty(), то переопределения свойств *.ssl ниже
        // нужны.
        // Рекомендуемый вариант: использовать ok http и другие подобные реализации
        // с явным созданием SSLContext и передачей его SSLSocketFactory в клиент
        // Ok http.
        // Если инициализировать провайдер в CSPConfig с помощью initEx(), то
        // свойства будут включены там, поэтому выше используется упрощенная
        // версия инициализации.
        //
        // Security.setProperty("ssl.KeyManagerFactory.algorithm",   Provider.KEYMANGER_ALG);
        // Security.setProperty("ssl.TrustManagerFactory.algorithm", Provider.KEYMANGER_ALG);
        //
        // Security.setProperty("ssl.SocketFactory.provider",       "ru.CryptoPro.ssl.SSLSocketFactoryImpl");
        // Security.setProperty("ssl.ServerSocketFactory.provider", "ru.CryptoPro.ssl.SSLServerSocketFactoryImpl");

        if (Security.getProvider(Provider.PROVIDER_NAME) == null) {
            Security.addProvider(new Provider());
        } // if

        //
        // Провайдер хеширования, подписи, шифрования
        // по умолчанию.
        //

        cpSSLConfig.setDefaultSSLProvider(JCSP.PROVIDER_NAME);

        //
        // Загрузка Revocation Provider (CRL, OCSP).
        //

        if (Security.getProvider(RevCheck.PROVIDER_NAME) == null) {
            Security.addProvider(new RevCheck());
        } // if

        //
        // Таймауты для CRL на всякий случай.
        //

        System.setProperty("com.sun.security.crl.timeout",  "5");
        System.setProperty("ru.CryptoPro.crl.read_timeout", "5");

        // Включаем возможность онлайновой проверки статуса
        // сертификата.
        //
        // Для TLS проверку цепочки сертификатов другой стороны
        // можно отключить, если создать параметр
        // Enable_revocation_default=false в файле android_pref_store
        // (shared preferences), см.
        // {@link ru.CryptoPro.JCP.tools.pref_store#AndroidPrefStore}.

        System.setProperty("com.sun.security.enableCRLDP", "true");
        System.setProperty("com.ibm.security.enableCRLDP", "true");

        // Включаем параметр tls_prohibit_disabled_validation - по
        // умолчанию он включен.
        // Параметр управляет несколькими настройками, когда включен:
        // * требует строгое использование ГОСТ алгоритмов в цепочке сертификатов другой стороны
        // при проверке;
        // * если не задан протокол для проверки на соответствие хоста и сертификата сервера, то
        // протокол по умолчанию - https;
        // * должен быть явно отключен при отключении проверки цепочки сертификатов другой стороны;
        // * проверяет имя провайдера на соответсвтие допустимым - JCP или JCSP.

        System.setProperty("tls_prohibit_disabled_validation", "true");

    }

    /************************ Поле для вывода логов *************************/

    /**
     * Инициализация объекта для отображения логов.
     *
     */
    private void initLogger() {

        // Поле для вывода логов и метка для отображения
        // статуса.

        EditText etLog = findViewById(R.id.etLog);
        etLog.setMinLines(10);

        TextView tvOpStatus = findViewById(R.id.tvOpStatus);

        Logger.init(getResources(), etLog, tvOpStatus);
        Logger.clear();

    }

    /************************** Служебные функции ****************************/

    /**
     * Информация о тестовых контейнерах.
     *
     */
    private void logTestContainers() {

        final String format = getString(R.string.ContainerAboutTestContainer);
        Logger.log("$$$ About test containers $$$");

        Logger.log(String.format(format, CLIENT_CONTAINER_2012_256_NAME,
                CLIENT_KEY_2012_256_PASSWORD));

    }

    /**
     * Копирование тестовых контейнеров для подписи,
     * шифрования, обмена по TLS из архива в папку
     * keys приложения.
     *
     */
    private void installContainers() {

        final CSPTool cspTool = new CSPTool(this);
        cspTool.getAppInfrastructure().copyContainerFromArchive(R.raw.keys);

    }

}
