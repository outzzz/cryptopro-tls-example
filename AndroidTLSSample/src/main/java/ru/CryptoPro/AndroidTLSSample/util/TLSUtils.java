package ru.CryptoPro.AndroidTLSSample.util;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.net.ssl.SSLContext;

import ru.CryptoPro.JCSP.JCSP;
import ru.CryptoPro.JCSP.support.BKSTrustStore;
import ru.CryptoPro.ssl.Provider;
import ru.CryptoPro.ssl.util.TLSContext;

public class TLSUtils implements Constants {

    /*
     * Создание SSL контекста.
     *
     * @param dataDir Путь к папке приложения.
     * @param useClientAuth True, если требуется аутентификация
     *  клиента.
     * @return готовый SSL контекст.
     * @throws Exception.
     */
    public static SSLContext createSSLContext(String dataDir, boolean useClientAuth) throws Exception {

        Logger.log("Creating SSL context...");

        Logger.log("Initiating key store. Loading containers. " +
                "Default container type: " + keyStoreType);

        SSLContext sslCtx;

        // Используется общее для всех хранилище корневых
        // сертификатов cacerts.

        final String trustStorePath = dataDir +
                File.separator + BKSTrustStore.STORAGE_DIRECTORY + File.separator +
                BKSTrustStore.STORAGE_FILE_TRUST;

        if (useClientAuth) {

            sslCtx = TLSContext.initAuthClientSSL(
                    Provider.PROVIDER_NAME, // провайдер, по умолчанию - JTLS
                    "TLSv1.2",              // протокол, по умолчанию - GostTLS
                    JCSP.PROVIDER_NAME,
                    keyStoreType,
                    CLIENT_CONTAINER_2012_256_NAME, // точный алиас ключа
                    BouncyCastleProvider.PROVIDER_NAME,
                    BKSTrustStore.STORAGE_TYPE,
                    new FileInputStream(trustStorePath),
                    String.valueOf(BKSTrustStore.STORAGE_PASSWORD),
                    null
            );

        } // if
        else {

            sslCtx = TLSContext.initClientSSL(
                    Provider.PROVIDER_NAME, // провайдер, по умолчанию - JTLS
                    null,              // протокол, по умолчанию - GostTLS
                    BouncyCastleProvider.PROVIDER_NAME,
                    BKSTrustStore.STORAGE_TYPE,
                    new FileInputStream(trustStorePath),
                    String.valueOf(BKSTrustStore.STORAGE_PASSWORD),
                    null
            );

        } // else

        Logger.log("SSL context completed.");
        return sslCtx;

    }

    /**
     * Чтение потока до конца заголовка.
     *
     * @param in входной поток
     * @param end конец заголовка
     * @return буфер (байтовый массив)
     * @throws IOException ошибки ввода-вывода
     */
    public static byte[] readHeader(InputStream in, byte[] end) throws IOException {

        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int conformity = 0;
        int next;

        Logger.log("Try reading (Client.readHeader)");

        do {

            next = in.read();
            if (next == -1)
                throw new IOException(" Client: Error reading HTTP header");

            baos.write(next);
            if (next == end[conformity])
                conformity++;
            else
                conformity = 0;

        } while (conformity != end.length);
        return baos.toByteArray();

    }

    /**
     * Чтение известного количества байтов.
     *
     * @param in InputStream
     * @param len length
     * @return буфер
     * @throws IOException ошибки ввода-вывода
     */
    public static byte[] readBody(InputStream in, int len) throws IOException {

        // Если есть размер сообщения (прочитан из хидеров),
        // то используем его.
        if (len > 0) {

            final byte[] buf = new byte[len];
            int next;
            int pos = 0;

            while (pos != len) {

                next = in.read();

                if (next == -1) {
                    throw new IOException("Error reading HTTP body");
                } // if

                buf[pos++] = (byte) next;
            } // while

            return buf;
        } // if
        // Если размера нет (такое бывает), то читаем, пока не получим
        // конец файла.
        else {

            ByteArrayOutputStream buf = new ByteArrayOutputStream();

            while (true) {

                int next = in.read();

                if (next == -1) {

                    // Ничего не прочитали, сразу конец файла.
                    if (buf.size() == 0) {
                        throw new IOException("Error reading HTTP body");
                    } // if

                    break;
                } // if

                buf.write((byte) next);
            } // while

            return buf.toByteArray();
        } // else
    }

    /**
     * Разбор ответа сервера и извлечение длины файла
     *
     * @param str строка ответа
     * @return длина файла
     * @throws IOException ошибки ввода-вывода
     */
    public static int parseAnswer(String str) throws IOException {

        final String[] split = str.split("\r\n");

        if (!split[0].equalsIgnoreCase("HTTP/1.0 200 OK") &&
                !split[0].equalsIgnoreCase("HTTP/1.1 200 OK"))
            throw new IOException(split[0]);

        int len = -1;
        for (int i = 1; i < split.length; i++)
            if (split[i].startsWith("Content-Length:")) {
                final String ss =
                        split[i].substring("Content-Length:".length()).trim();
                len = Integer.parseInt(ss);
                break;
            }

        return len;

    }

}
