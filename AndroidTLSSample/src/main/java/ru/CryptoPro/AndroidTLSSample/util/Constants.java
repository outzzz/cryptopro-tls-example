/**
 * Copyright 2004-2013 Crypto-Pro. All rights reserved.
 * Программный код, содержащийся в этом файле, предназначен
 * для целей обучения. Может быть скопирован или модифицирован
 * при условии сохранения абзацев с указанием авторства и прав.
 *
 * Данный код не может быть непосредственно использован
 * для защиты информации. Компания Крипто-Про не несет никакой
 * ответственности за функционирование этого кода.
 */
package ru.CryptoPro.AndroidTLSSample.util;

import ru.CryptoPro.JCSP.JCSP;

/**
 * Служебный класс Constants с перечислением глобальных
 * констант клиентского приложения.
 *
 * 27/05/2013
 *
 */
public interface Constants {

    /**
     * Тэг логгера приложения.
     */
    String APP_LOGGER_TAG = "AndroidTLSSample";

    /**
     * Кодировка по умолчанию для html-страниц в TLS-примерах.
     */
    String DEFAULT_ENCODING = "windows-1251";

    /**
     * Название контейнера для подписи/шифрования.
     */
    String CLIENT_CONTAINER_2012_256_NAME = "cli12256";

    /**
     * Пароль ключа подписи/шифрования.
     */
    String CLIENT_KEY_2012_256_PASSWORD = "2";

    /**
     * Тип контейнера.
     */
    String keyStoreType = JCSP.HD_STORE_NAME;

    /**
     * Удаленный хост, поддерживающий новую cipher suite
     * (ГОСТ 2001, ГОСТ 2012), короткий хеш ГОСТ 2012, без
     * клиентской аутентификации.
     */
    String host2012256NoAuth = "https://tlsgost-256.cryptopro.ru/index.html";

    /**
     * Удаленный хост, поддерживающий новую cipher suite
     * (ГОСТ 2001, ГОСТ 2012), короткий хеш ГОСТ 2012, с
     * клиентской аутентификацией.
     */
    String host2012256ClientAuth = "https://tlsgost-256auth.cryptopro.ru/index.html";

    /**
     * Https-порт по умолчанию.
     */
    int DEFAULT_PORT = 443;

    int DEFAULT_TIMEOUT = 1000;

    String http_header_separator = "\r\n\r\n";

}
